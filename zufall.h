//zufall.h
#include <random>


#ifndef zufall_h
#define zufall_h zufall_h

class Zufall
{
	// Zufallsgenerator definieren
	std::default_random_engine generator;
	std::uniform_int_distribution<int> verteiler; 
public:
	
	Zufall(int von, int bis);
	// obj() gibt die Zufallszahl zurück
	int operator()(); 
};


#endif // ifndef zufall_h

//spieler.cpp

#include "spieler.h"

Menschlicher_Spieler::Menschlicher_Spieler
			(unsigned int von, unsigned int bis)
{
	this->von = von;
	this->bis = bis;
}

void Menschlicher_Spieler::zahl_ueberlegen()
{
	ausgabe.set_message
	(
		std::string("Bitte überlege dir eine Zahl\n")
		+ "im Bereich von "
		+ std::to_string(von)
		+ " bis "
		+ std::to_string(bis)
		+ "."
	);
	frage.setMenu
	(
		"Bitte gib ein <J> ein und drücke ENTER, wenn du soweit bist.\n",
		"jJ"
	);
	ausgabe.print();
	frage.get_char();
}

unsigned int Menschlicher_Spieler::raten()
{
	zahleneingabe.set_message
	(
		std::string("Die Zahl liegt im Bereich von ")
		+ std::to_string(von)
		+ " bis " 
		+ std::to_string(bis)
		+ ".\n"
		+ "Welche Zahl kann das sein? :>"
	);
	
	return zahl = zahleneingabe.get_int();
}

Bewertung Menschlicher_Spieler::bewerte
			(unsigned int tipp)
{
	Bewertung wertung;
	ausgabe.set_message
	(
		std::string("Ist es vielleicht die ")
		+ std::to_string(tipp)
		+ "?"
	);
	frage.setMenu
	(
		std::string("Liege ich mit dem Tipp\n")
		+ "1) richtig\n"
		+ "2) zu hoch\n"
		+ "3) zu niedrig\n"
		+ "? :>",
		"123"
	);
	ausgabe.print();
	switch ( frage.get_char() )
	{
		case '1':
			wertung = Bewertung::RICHTIG;
			break;
		case '2':
			wertung = Bewertung::ZU_HOCH;
			break;
		case '3':
			wertung = Bewertung::ZU_NIEDRIG;
	}
	
	return wertung;
}

bool Menschlicher_Spieler::auswertung
			(Bewertung wertung)
{
	bool richtig = false;
	std::string status;
	switch (wertung)
	{
		case Bewertung::RICHTIG:
			richtig= true;
			status = "richtig! Gratuliere!";
			break;
		case Bewertung::ZU_HOCH:
			status = "zu hoch!\nVersuche es doch mal mit einer kleineren Zahl!";
			break;
		case Bewertung::ZU_NIEDRIG:
			status= "zu niedrig!\nVersuche es doch mal mit einer größeren Zahl!";
	}
	
	ausgabe.set_message
	(
		std::string("Die Zahl ")
		+ std::to_string(zahl)
		+ " ist "
	  + status
	);
	ausgabe.print();
	return richtig;
}

bool Menschlicher_Spieler::willst_du_raten()
{
	bool antwort = false;
	ausgabe.set_message
	(
		std::string("Wir spielen Zahlenraten im Bereich von ")
		+ std::to_string(von)
		+ " bis " 
		+ std::to_string(bis)
		+ "."
	);
	ausgabe.print();
	frage.setMenu
	(
		"Möchtest du raten? (J/N) :>",
		"jJnN"
	);
	switch (frage.get_char())
	{
		case 'J':
		case 'j':
			antwort = true;
	}
	ausgabe.set_message("");
	ausgabe.print();
	
	return antwort;
}

bool Menschlicher_Spieler::wiederholen()
{
	bool antwort = false;
	std::cout
	<< "Zahlenraten im Bereich von "
	<< std::to_string(von)
	<< " bis " 
	<< std::to_string(bis)
	<< " hat Spaß gemacht."
	<< std::endl;
	
	frage.setMenu
	(
		"Wollen wir noch einmal spielen? (J/N) :>",
		"jJnN"
	);
	switch (frage.get_char())
	{
		case 'J':
		case 'j':
			antwort = true;
	}
	
	return antwort;
}

Computer_Spieler::Computer_Spieler
			(unsigned int v, unsigned int b)
	: zufallszahl{new Zufall(v,b)},
		min{v-1},max{b+1}
{
	this->von = v-1;
	this->bis = b+1;
}

void Computer_Spieler::zahl_ueberlegen()
{
	zahl = (*zufallszahl)();
}

unsigned int Computer_Spieler::raten()
{
	return zahl = (min+max)/2;
}

Bewertung Computer_Spieler::bewerte
			(unsigned int tipp)
{
	return tipp == zahl ? Bewertung::RICHTIG
				:tipp  > zahl ? Bewertung::ZU_HOCH
											: Bewertung::ZU_NIEDRIG;
	
}

bool Computer_Spieler::auswertung
			(Bewertung wertung)
{
	bool richtig = false;
	
	switch (wertung)
	{
		case Bewertung::RICHTIG:
			richtig = true;
			break;
		case Bewertung::ZU_HOCH:
			max = zahl;
			break;
		case Bewertung::ZU_NIEDRIG:
			min = zahl;
	}
	
	if (max-min < 2)
	{
		std::cout 
		<< "Es gibt keine natürliche Zahl zwischen "
		<< min << " und " << max << "!\n";
		richtig = true;
	}
	if(richtig)
	{
		min = von;
		max = bis;
		
		std::cout
		<< "Haha..., ich habe es erraten!"
		<< std::endl;
	}
	
	return richtig;
}

//input.cpp
#include "input.h"

Menu::Menu() 
	:txtMenu("Eingabe: "),
	erlaubteEingaben("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ,;.:-_#'+*~´`ß?\\=})]([/{&%$§\"!^′°<>@€äöüÄÖÜ")
	{}

Menu::Menu
	(std::string f, std::string eE)
  : txtMenu(f), erlaubteEingaben(eE)
{}

void Menu::setMenu(std::string f, std::string eE){
		txtMenu=f;
		erlaubteEingaben=eE;
}

char Menu::get_char() const{
	char wahl=0;
	while (!wahl)
	{
			std::cout << txtMenu;
			std::cin >> wahl;
			
			//Behandlung falsche Eingabe
			if(	erlaubteEingaben.find(wahl) 
								== std::string::npos)
			{
				wahl=0;
			}
		
		// Eingabepuffer löschen	
		std::cin.ignore
				(std::numeric_limits<std::streamsize>::max()
				,'\n');
	}
	return wahl;
}

short Menu::get_short() const{
		return ( get_char() - CHAR_TO_SHORT );
}

Int_in::Int_in()
	: message{"Zahl: "}
{}

Int_in::Int_in(std::string ea)
	: message{ea}
{}

void Int_in::set_message(std::string ea){
		message = ea;
}

int Int_in::get_int() const{
	int in = 0;
	
	// Ausnahmen einschalten
  std::cin.exceptions(std::cin.failbit | std::cin.badbit);
  do
  {
		try
			{
				std::cout << message;
				std::cin >> in;
			}
			catch (std::istream::failure &e)
			{
				in=0;
				std::cin.clear();
			}
			//Eingabepuffer bis zum Enter leeren
			std::cin.ignore
					(std::numeric_limits<std::streamsize>::max()
					,'\n');
	}while(!in);
	
	return in;
}

//zahlenraten-main.cpp

#include <iostream>
#include "zahlenraten.h"

int main(int argc, char* argv[]){
	
	Head ausgabe
				(
					"Zahlenraten",
					"Lass uns ein kleines Zahlenratenspiel spielen."
				);
	Int_in untergrenze
					( "Untergrenze :>" );
	Int_in obergrenze
					( "Obergrenze  :>" );
	unsigned int ug,og;
	ausgabe.print();
	std::cout
		<< "In welchem Zahlenbereich wollen wir spielen?"
		<< std::endl;
		
	do
	{
		ug = untergrenze.get_int();
		og = obergrenze.get_int();
	}while ( !(og>ug));
	
	Zahlenraten zahlenratenspiel{ug,og};
	zahlenratenspiel.play();
	
	ausgabe.set_message	
		(
			std::string("Das hat Spaß gemacht\n")
			+ "Bis zum nächsten Mal. :-)\n"
			+ "Bye!"
		);
	ausgabe.print();

	return 0;
}

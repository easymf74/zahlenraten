//spieler.h
#ifndef SPIELER_H
#define SPIELER_H SPIELER_H

#include "output.h"
#include "input.h"
#include <memory>
#include "zufall.h"

enum class Bewertung{
	RICHTIG, ZU_HOCH, ZU_NIEDRIG
};

class Spieler
{
	public:
		virtual ~Spieler(){};
		virtual void zahl_ueberlegen()			=0;
		virtual unsigned int raten()				=0;
		virtual Bewertung bewerte
			(unsigned int tipp)								=0;
		virtual bool auswertung
			(Bewertung wertung)								=0;
	protected:
		Head ausgabe{"Zahlenraten", ""};
		unsigned int zahl, von, bis;
};

class Menschlicher_Spieler : public Spieler
{
	public:
		Menschlicher_Spieler
			(unsigned int von, unsigned int bis);
		void zahl_ueberlegen() override;
		unsigned int raten() override;
		Bewertung bewerte
			(unsigned int tipp) override;
		bool auswertung
			(Bewertung wertung)	override;
		bool willst_du_raten();
		bool wiederholen();
	private:
		Menu frage;
		Int_in zahleneingabe;
};

class Computer_Spieler : public Spieler
{	
	public:
		Computer_Spieler
			(unsigned int v, unsigned int b);
		void zahl_ueberlegen() override;
		unsigned int raten() override;
		Bewertung bewerte
			(unsigned int tipp) override;
		bool auswertung
			(Bewertung wertung)	override;
	private:
		std::unique_ptr<Zufall> zufallszahl;
		unsigned int min, max;
};
#endif //ifndef SPIELER_H

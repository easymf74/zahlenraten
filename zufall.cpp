//zufall.cpp
#include "zufall.h"

Zufall::Zufall(int von,int bis)
 : verteiler(std::uniform_int_distribution<int>(von,bis))
{
	// Startwert zeitabhängig setzen
	generator.seed( time(nullptr)*time(nullptr) );
}


int Zufall::operator()()
{ 
	return verteiler(generator);
}

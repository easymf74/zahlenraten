# *-* Makefile *-*

C=g++
CF=-c -std=c++14 -Wall

HD=zufall.h\
		input.h\
		output.h\
		systembefehle.h\
		spieler.h\
		zahlenraten.h

OBJ=zufall.o\
		input.o\
		output.o\
		spieler.o\
		zalenraten.o\
		zalenraten-main.o
		
all: zahlenraten

zahlenraten: $(OBJ)
	$(C) $^ -o $@
	
zufall.o:	zufall.cpp zufall.h
	$(C) $(CF) $< -o $@

input.o: input.cpp input.h
	$(C) $(CF) $< -o $@
	
output.o: output.cpp output.h
	$(C) $(CF) $< -o $@

spieler.o: spieler.cpp spieler.h input.h output.h zufall.h
	$(C) $(CF) $< -o $@

zalenraten.o: zahlenraten.cpp zahlenraten.h spieler.h input.h output.h zufall.h
	$(C) $(CF) $< -o $@
	
zalenraten-main.o: zahlenraten-main.cpp $(HD)
	$(C) $(CF) $< -o $@
	
clean:
	$(RM) $(OBJ)


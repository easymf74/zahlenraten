//zahlenraten.cpp
#include "zahlenraten.h"

Zahlenraten::Zahlenraten
	(unsigned int von, unsigned int bis)
	: mensch{new Menschlicher_Spieler(von,bis)},
	  computer{new Computer_Spieler(von,bis)}
{}

void Zahlenraten::play()
{
	do
	{
		if(mensch->willst_du_raten())
		{
			bewerter = computer.get();
			rater 	 = mensch.get();
		}else
		{
			bewerter = mensch.get();
			rater    = computer.get();
		}
		
		bewerter->zahl_ueberlegen();
		do
		{
			++anzahl_versuche;
		}while
			(
				!rater->auswertung
					(bewerter->bewerte
						(rater->raten() ))
			);
		std::cout
		<< "\nDie Zahl wurde in "
		<< anzahl_versuche
		<< " Versuchen erraten!"
		<< std::endl;
		anzahl_versuche=0;
	}while( mensch->wiederholen() );
}
	
	

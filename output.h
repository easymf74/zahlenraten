//output.h
#ifndef OUTPUT_H
#define OUTPUT_H OUTPUT_H
#include "systembefehle.h"
#include <iostream>
#include <string>

class Head
{
	public:
		Head();
		Head(std::string head,std::string txt);
		void set_message(std::string);
		void set_head(std::string);
		void print();
	private:
		std::string headline;
		std::string message;
};

#endif // OUTPUT_H

//zahlenraten.h
#ifndef ZAHLENRATEN_H
#define ZAHLENRATEN_H ZAHLENRATEN_H

#include "spieler.h"

class Zahlenraten
{
	public:
		Zahlenraten
			(unsigned int von, unsigned int bis);
		void play();
	private:
		std::unique_ptr<Menschlicher_Spieler> mensch;
		std::unique_ptr<Computer_Spieler> computer;
		Spieler* bewerter=nullptr;
		Spieler* rater   =nullptr;
		unsigned int anzahl_versuche =0;
};

#endif // ifndef ZAHLENRATEN_H
